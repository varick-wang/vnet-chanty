# 																			LanProxy实现内网穿透

一、 [准备](#一、准备)

二、 [香山机房服务端配置](#二、香山机房服务端配置)

1. [安装java](#安装java)
2. [安装lanproxy](#安装lanproxy)
3. [使用](#使用(图片仅参考))

三、[客户端配置](#客户端配置（机房外设备）)

![lanproxy](D:\VK\lanproxy\lanproxy.png)

## 一、准备



这里使用java客户端与服务端连接（以北京香山机房为例）

1、一台公网服务器（运行proxy-server）（香山机房公网IP地址：124.65.189.166，内网IP地址：192.168.10.10）。
2、一台内网客户端（运行proxy-client）（香山机房环控服务器，内网地址：192.168.10.11）。

Lanproxy下载地址：https://seafile.cdjxt.net/d/2e81550ebdbd416c933f/

![img](https://img2018.cnblogs.com/blog/1560319/201909/1560319-20190903111945562-2023280479.png)

---



## 二、香山机房服务端配置

###  安装java

1、删除自带jdk

```go
rpm -e --nodeps `rpm -qa | grep java`
```

2、查看yum库中有哪些jdk版本。
`yum search java | grep jdk`

3、选择java-1.8.0-openjdk-devel.x86_64 : OpenJDK Development Environment版本进行安装（也可以使用更高版本安装）。
`yum -y install java-*版本号*-openjdk-devel.x86_64`

默认安装目录为`/usr/lib/jvm/java-*版本号*-openjdk-*版本号*.x86_64`。

4、更改配置文件，环境变量
`vim /etc/profile`

在最后添加：

```ruby
#set java environment
#与安装版本相关（JAVA_HOME=默认安装目录），其他默认
JAVA_HOME=/usr/lib/jvm/java-*版本号*-openjdk-*版本号*.x86_64
JRE_HOME=$JAVA_HOME/jre
CLASS_PATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib
PATH=$PATH:$JAVA_HOME/bin:$JRE_HOME/bin
export JAVA_HOME JRE_HOME CLASS_PATH PATH
```

5、更新配置，让修改立即生效
`source /etc/profile`

6、输入相关命令，查看安装结果
`java`，`javac`，`java -version`

----

## 安装lanproxy

1、访问[lanproxy下载地址](https://github.com/ffay/lanproxy/releases)，下载proxy-server\*版本号*.zip，上传到服务器。准备环境已经准备好包了，直接传到服务器即可，没有查看[第一步](一、准备)

2、解压安装

```shell
unzip proxy-server-*版本号*.zip
mv proxy-server-*版本号* /usr/local/
```

3、修改配置文件
`vim /usr/local/proxy-server-*版本号*/conf/config.properties`
修改管理员的用户名和密码。（这个随意）

```shsh
#服务地址
server.bind=192.168.10.10
#服务端口
server.port=4900

#ssl配置可以默认
server.ssl.enable=ture
server.ssl.bind=192.168.10.10
server.ssl.port=4993
server.ssl.jksPath=test,jks
server.ssl.keyStorePassword=123456
server.ssl.keyManagerPassword=123456
server.ssl.needsClientAuth=false

#服务页面管理访问地址
config.server.bind=192.168.10.10
#服务页面访问端口
config.server.port=8090
#服务页管理访问登录用户名
config.admin.username=admin
#服务页管理访问登录用户名密码
config.admin.password=admin
```

4、启动服务

```shsh
cd /usr/local/proxy-server-*版本号*/bin
chmod +x startup.sh
./startup.sh
```

5、访问 http://192.168.10.10:8090 ，即可看到登录界面。

 

![img](https://upload-images.jianshu.io/upload_images/1529081-fbad600ee3efa350.jpg)

 

##  

### 使用(图片仅参考)

1. 服务端配置
   首先添加一个客户端:
   ![img](https://img2018.cnblogs.com/blog/1560319/201909/1560319-20190904094248542-511890697.png)

   添加成功后在客户端管理查看添加的客户端:
   ![img](https://img2018.cnblogs.com/blog/1560319/201909/1560319-20190904094410136-1364232782.png)

   这步完成后，其实就可以配置客户端了，来验证添加的服务端能否显示在线

   然后在对添加成功的客户端进行配置:
   ![img](https://img2018.cnblogs.com/blog/1560319/201909/1560319-20190904094827940-1855937642.png)

> **等我们配置完客户端就可以直接通过　　\*.\*.\*.115:5001　　来访问内网地址　　\*.\*.\*.201:8090**	（此IP图片内容，仅参考）
>
> ​																	例如：通过  124.65.189.166：5001     访问   192.168.10.10：8090

 

> 一个服务端代理可以配置多个内网服务端口

- 代理名称，推荐输入客户端要代理出去的端口，或者是客户端想要发布到公网的项目名称。
- 公网端口，填入一个公网服务器空闲端口，用来转发请求给客户端。
- 代理IP端口，填入内网服务器IP+端口，公网会转发请求给该客户端端口。

---

 

## 客户端配置（机房环控服务器）

1、访问[lanproxy下载地址](https://github.com/ffay/lanproxy/releases)，下载proxy-client-\*版本号*.zip，解压到喜欢的目录。

2、进入proxy-client-\*版本号*/conf目录，修改config.properties为：

![img](https://img2018.cnblogs.com/blog/1560319/201909/1560319-20190904095735824-1823850054.png)

3、进入proxy-client-\*版本号*/bin目录，执行 ./startup.sh，即可启动lanproxy客户端。

> ./ startup.sh　　#启动
>
>  ./stop.sh　　　#停止

如果启动失败，一般是因为jdk没有安装配置成功，参考[《](http://www.voidking.com/2017/05/03/deve-idea-config/)[安装java1.8.0](https://www.cnblogs.com/wei9593/p/11417188.html)[》](http://www.voidking.com/2017/05/03/deve-idea-config/)中的安装jdk，安装配置jdk后再次启动即可。

4、个人PC访问地址 124.65.189.166：5001 ，即可看到本地访问客户端80端口相同的页面。

 

![img](https://img2018.cnblogs.com/blog/1560319/201909/1560319-20190904100730779-1585498731.png)

至此，代理成功！