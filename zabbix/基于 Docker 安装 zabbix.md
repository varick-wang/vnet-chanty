# 基于 Docker 安装 zabbix

docker+zabbix，使用docker搭建zabbix服务

## Zabbix 介绍

　　zabbix（音同 zæbix）是一个基于WEB界面的提供分布式[系统监视](https://baike.baidu.com/item/系统监视)以及网络监视功能的企业级的开源解决方案。

　　zabbix能监视各种网络参数，保证[服务器系统](https://baike.baidu.com/item/服务器系统)的安全运营；并提供灵活的通知机制以让[系统管理员](https://baike.baidu.com/item/系统管理员)快速定位/解决存在的各种问题。

　　Zabbix  作为企业级分布式监控系统，具有很多优点，如：分布式监控，支持 node 和 proxy  分布式模式；自动化注册，根据规则，自动注册主机到监控平台，自动添加监控模板；支持 agentd、snmp、ipmi 和 jmx  等很多通信方式。然而部署一套完整的zabbix，需要安装数据库、web服务器、zabbix-server和zabbix-agent，这几样服务，过程都比较复杂，一不留心就可能出错，所以今天我使用docker容器搭建了一套zabbix服务，不仅搭建快，而且不易出错。本文适合了解docker的朋友。

　　zabbix中文文档https://www.zabbix.com/documentation/3.4/zh/start

## Zabbix in Docker

### 一、Zabbix 相关镜像下载

1. 下载载MySQL镜像[mysql:5.7](https://hub.docker.com/_/mysql/)。在docker hub上有以下版本

  8.0.11，8.0，latest

  5.7.22，5.7，5

  5.6.40, 5.6 

  5.5.60, 5.5

这里使用5.7版本，在linux终端拉取mysql5.7镜像

 ```bash
 # docker pull mysql:5.7
 ```

查看下载的镜像

 ```bash
 # docker image ls
 ```

![img](D:\VK\git\chanty\zabbix\image\docker-images.png)

2. 下载zabbix-server镜像，zabbix-server镜像分两种，支持MySQL数据库[zabbix-server-mysql](https://hub.docker.com/r/zabbix/zabbix-server-mysql/)，支持支持PostgreSQL数据库[zabbix/zabbix-server-pgsql](https://hub.docker.com/r/zabbix/zabbix-server-pgsql/)。下面安装的是支持MySQL数据库的Server镜像。

   打开zabbix-server-mysql的docker hub，大家会发现，zabbix-server-mysql有下面这些版本
```
1 Zabbix server 3.0 (tags: alpine-3.0-latest, ubuntu-3.0-latest, centos-3.0-latest)    
2 Zabbix server 3.0.* (tags: alpine-3.0.*, ubuntu-3.0.*, centos-3.0.*) 
3 Zabbix server 3.2 (tags: alpine-3.2-latest, ubuntu-3.2-latest, centos-3.2-latest)    
4 Zabbix server 3.2.* (tags: alpine-3.2.*, ubuntu-3.2.*, centos-3.2.*)   
5 Zabbix server 3.4 (tags: alpine-3.4-latest, ubuntu-3.4-latest,  centos-3.4-latest, alpine-latest, ubuntu-latest, centos-latest, latest) 
6 Zabbix server 3.4.* (tags: alpine-3.4.*, ubuntu-3.4.*, centos-3.4.*)   
7 Zabbix server 4.0 (tags: alpine-trunk, ubuntu-trunk)  
```
因为我的服务器是centos7版本，所以选择的是centos-latest版本。还有后面如果想自己重做镜像的话，选择centos版本，用着会更简单一点。在linux终端使用
```bash
# docker pull zabbix/zabbix-server-mysql:centos-latest
```
下载zabbix-server的镜像。查看下载的镜像
```bash
# docker image ls
```
![img](https://images2018.cnblogs.com/blog/965054/201807/965054-20180724171909684-887839245.png)

3. 下载Zabbix web镜像，这里使用的是基于Nginx web服务器及支持MySQL数据库的Zabbix web接口zabbix/zabbix-web-nginx-mysql。这里是用的是latest版本，在linux终端使用
```bash
# docker pull zabbix/zabbix-web-nginx-mysql:latest
```
下载web镜像。查看下载到的镜像

![img](https://images2018.cnblogs.com/blog/965054/201807/965054-20180724172034856-1712700179.png)

\4. 下载[zabbix-java-gateway](https://hub.docker.com/r/zabbix/zabbix-java-gateway/)镜像， Zabbix本身不支持直接监控Java，而是使用zabbix-java-gateway监控jvm/tomcat性能。这里我们使用latest版本，在linux终端使用

 1 docker pull zabbix/zabbix-java-gateway:latest

下载zabbix-java-gateway镜像。查看下载镜像

 1 docker image ls

![img](https://images2018.cnblogs.com/blog/965054/201807/965054-20180724172904018-1105373957.png)

- **镜像运行**

\1. 启动zabbix等镜像之前，需要先创建一个新的 Docker 网络。需要将后面的zabbix-server、mysql、web等容器都加入到此网络中，方便互相访问。在终端使用下面命令创建。

 1 docker network create -d bridge zabbix_net

创建后，可以查看是否创建成功。

 1 docker network ls

\2. 运行mysql 镜像，创建mysql容器。

 1 docker run -dit -p 3306:3306 --name zabbix-mysql --network zabbix_net  --restart always -v /etc/localtime:/etc/localtime -e MYSQL_DATABASE="zabbix" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="zabbix123" -e MYSQL_ROOT_PASSWORD="root123" mysql:5.7 

> `MYSQL_DATABASE=``"zabbix"`         在msql中创建的数据库的名
>
> ```
> MYSQL_USER=``"zabbix"``       创建msql的登录账户名
> MYSQL_PASSWORD=``"zabbix123"` `   设置创建msql的登录账户的密码
> MYSQL_ROOT_PASSWORD=``"root123"  设置msql数据库root 的密码
> ```

其中-p 是将容器中的3306端口映射到服务器的3306端口，

--network zabbix_net是将容器加入到zabbix_net网络中，

-v /etc/localtime:/etc/localtime是同步服务器和容器内部的时区，

--restart always设置自启动，

-e MYSQL_DATABASE="zabbix"，创建环境变量。

--name zabbix-mysql，给容器命名。

\3. 运行zabbix-java-gateway镜像，创建zabbix-java-gateway容器。

 1 docker run -v /etc/localtime:/etc/localtime -dit --restart=always  --name=zabbix-java-gateway --network zabbix_net  zabbix/zabbix-java-gateway:latest 

\4. 运行zabbix-server-mysql镜像，创建zabbix-server-mysql容器。

首先创建数据卷zabbix-server-vol，通过命令

 1 docker volume create zabbix-server-vol

启动zabbix-server-mysql容器。

> ```
> 此处的以下内容与 运行mysql 镜像，创建mysql容器设置的内容要一致
> MYSQL_DATABASE=``"zabbix"
> MYSQL_USER=``"zabbix"`` 
> MYSQL_PASSWORD=``"zabbix123"
> MYSQL_ROOT_PASSWORD=``"root123"
> ```

 1 docker run -dit -p 10051:10051 --mount  source=zabbix-server-vol,target=/etc/zabbix -v  /etc/localtime:/etc/localtime -v  /usr/lib/zabbix/alertscripts:/usr/lib/zabbix/alertscripts  --name=zabbix-server-mysql --restart=always --network zabbix_net -e  DB_SERVER_HOST="zabbix-mysql" -e MYSQL_DATABASE="zabbix" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="zabbix123" -e MYSQL_ROOT_PASSWORD="root123" -e ZBX_JAVAGATEWAY="zabbix-java-gateway" zabbix/zabbix-server-mysql:centos-latest

> 需要悉知各个配置

\5. 运行zabbix-web-nginx-mysql镜像，创建zabbix-web-nginx-mysql容器。

> ```
> 此处的以下内容与 运行mysql 镜像，创建mysql容器设置的内容要一致
> MYSQL_DATABASE=``"zabbix"
> MYSQL_USER=``"zabbix"`` 
> MYSQL_PASSWORD=``"zabbix123"
> MYSQL_ROOT_PASSWORD=``"root123"
> ```

 1 docker run -dit -p 8080:8080 -v /etc/localtime:/etc/localtime --name  zabbix-web-nginx-mysql --restart=always --network zabbix_net -e  DB_SERVER_HOST="zabbix-mysql" -e MYSQL_DATABASE="zabbix" -e MYSQL_USER="zabbix" -e MYSQL_PASSWORD="zabbix123" -e MYSQL_ROOT_PASSWORD="root123" -e ZBX_SERVER_HOST="zabbix-server-mysql" zabbix/zabbix-web-nginx-mysql:latest 

zabbix所需容器已经全部启动

 1 docker ps

![img](https://images2018.cnblogs.com/blog/965054/201807/965054-20180724180554725-801009030.png)

\6. 在浏览器中输入http://IP/zabbix，打开zabbix首页，其中用户名密码分别是admin/zabbix。

出现下面页面，zabbix搭建成功。是不是比自己创建数据库，搭建zabbix-server简单很多。

[![image](https://img2020.cnblogs.com/blog/1424989/202004/1424989-20200403111924297-714473788.png)](https://img2020.cnblogs.com/blog/1424989/202004/1424989-20200403111921230-1569285283.png)

 

 