```shell
#/bin/bash
#配置禁用ROOT账户登录
sed -i 's#root:x:0:0:root:/root:/bin/bash#root:x:0:0:root:/root:/sbin/nologin#'  /etc/passwd

#配置禁用无用账户登录
sed -i 's#centos:x:1000:1000:Cloud User:/home/centos:/bin/bash#centos:x:1000:1000:Cloud User:/home/centos:/sbin/nologin#'    /etc/passwd

#配置修改危险文件（命令）权限
chmod 700  /usr/bin/rm
chmod 700 /usr/bin/mv

#配置禁止ROOT远程登录
sed -i 's/#PermitRootLogin yes/PermitRootLogin no/'    /etc/ssh/sshd_config

#配置更改SSH默认端口
sed -i 's/#Port 22/Port 2211/'    /etc/ssh/sshd_config

#配置新建非ROOT的管理员账户
useradd hermes 
echo 'Cozy@1026$6@!' | passwd hermes --stdin 
echo "hermes    ALL=(ALL)       ALL"  >>  /etc/sudoers

#配置用户使用SUDO权限
useradd jasperf 
echo 'xxx@1026$6@!' | passwd jasperf --stdin 
echo "jasperf    ALL=/usr/sbin/*,/sbin/*,/usr/bin/*,!/bin/chattr,!/bin/passwd" >>  /etc/sudoers

#配置添加口令策略
#更改密码使用90天，密码长度至少为8位
sed -i.bak -e 's/^\(PASS_MAX_DAYS\).*/\1   90/' /etc/login.defs
sed -i.bak -e 's/^\(PASS_MIN_LEN\).*/\1   8/' /etc/login.defs
#更改后的策略
#PASS_MAX_DAYS 90 #新建用户的密码最长使用天数
#PASS_MIN_DAYS 0 #新建用户的密码最短使用天数
#PASS_MIN_LEN  8  #新建用户的密码最少字符 
#PASS_WARN_AGE 7 #新建用户的密码到期提前提醒天数
#密码至少包含一个数字、一个小写字母、一个大写字母、一个特殊字符
#18行添加一行空行
sed -i '18G'     /etc/pam.d/system-auth
sed -i '18 i password    requisite     pam_cracklib.so try_first_pass retry=5 dcredit=-1 lcredit=-1 ucredit=-1 ocredit=-1 minlen=8'     /etc/pam.d/system-auth

#配置用户登录提醒
cat >>  /etc/ssh/login_notifiy.sh   <>     /etc/pam.d/sshd

#配置开启审计
systemctl start rsyslogd
systemctl start auditd

#配置开启日志存储6个月
sed -i 's/weekly/monthly/' /etc/logrotate.conf
sed -i 's/rotate 4/rotate 6/g' /etc/logrotate.conf
sed -i 's/rotate 1/rotate 6/g' /etc/logrotate.conf

#配置超时锁定
#登录超时设置15分钟
echo "exprot TMOUT=900"  >>  /etc/profile

#配置防止拒绝服务攻击
echo "net.ipv4.tcp_max_syn_backlog = 2048" >> /etc/sysctl.conf

================================================================================
#配置防止暴力破解SSH攻击
#安装DenyHost
tar xf DenyHosts-3.0..tar.gz
cd DenyHosts-3.0
python setup.py install
cp denyhosts.cfg-dist /etc/denyhosts.cfg
cp daemon-control-dist daemon-control
chown xxx  daemon-control
chmod 700 daemon-control
sed -i 's#/usr/share/denyhosts/denyhosts.cfg#/etc/denyhosts.cfg#'   daemon-control

#配置保存1万条历史记录
#增加如下，history格式更改为（行数  年-月-日 时-分-秒   IP    用户  xxx命令） 
vim /etc/profile  
HISTSIZE=10000
USER_IP=`who -u am i 2>/dev/null| awk '{print $NF}' |sed -e 's/[()]//g'`
if [ "$USER_IP" = "" ]
then
USER_IP=`hostname`
fi
export HISTTIMEFORMAT="%F %T $USER_IP `whoami` "
shopt -s histappend
export PROMPT_COMMAND="history -a"

#配置安装杀毒软件
mkdir -p agent_tmp && cd agent_tmp && curl -o wsssr_defence_agent_8.0.2.124_64bit.tar.gz 172.22.220.15:7901/file/dl/39081adcac0dbb9d588a6cb83353559fdc9bd8031e41eb53321b8daef1a21b58?dir=installPkg && tar -xzvf wsssr_defence_agent_8.0.2.124_64bit.tar.gz && chmod +x ./*_install/install && ./*_install/install -c 172.22.220.15 -U da7a0748bb4f4909aebf9e45d5ce5648 -d 

#配置仅堡垒机访问
Openstack控制台，设置Security Group，Default，SSH(184,139)

echo #!/bin/bash

[ "$PAM_TYPE" = "open_session" ] || exit 0
{
echo "User: $PAM_USER"
echo "Ruser: $PAM_RUSER"
echo "Rhost: $PAM_RHOST"
echo "Service: $PAM_SERVICE"
echo "TTY: $PAM_TTY"
echo "Date: `date`"
echo "Server: `uname -a`"
}
```

