## Big data bastion machine scheme.

​		In big data environment, it is necessary to add a bastion machine to improve manageability and security.

​		At present, there are two schemes (including annexes):

1. Create an instance through our private cloud and connect to the switch as a bastion machine.
2. Through virtualization technology, the virtual machine is turned on as a bastion machine on each big data device, and is connected to the switch.
3. .…………

