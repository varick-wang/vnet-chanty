# Zabbix安装部署（版本4.0.33）



## 一、环境准备

### 1.部署LNMP/LAMP基础环境（使用nginx 1.20.1）

#### 注：Zabbix监控管理控制台需要通过Web页面展示出来，并且还需要使用MySQL来存储数据，因此需要先为Zabbix准备基础LNMP环境。

```shell
[root@zabbixserver ~]# yum -y install gcc pcre-devel  openssl-devel

[root@zabbixserver ~]# tar -xf nginx-1.12.2.tar.gz

[root@zabbixserver ~]# cd nginx-1.12.2

[root@zabbixserver nginx-1.12.2]# ./configure --with-http_ssl_module

[root@zabbixserver nginx-1.12.2]# make && make install

[root@zabbixserver ~]# yum -y  install  php  php-mysql  php-fpm

[root@zabbixserver ~]# yum -y  install  mariadb  mariadb-devel  mariadb-server 
```

​	2.配置环境&源码安装zabbix

