```yaml
---
- hosts: bigdate_new
  tasks: 
    - name: 循环创建jupiter\neptune
      user: 
        name: "{{item.uname}}"
        password: "{{item.upass | password_hash('sha512')}}"
      loop: 
        - { uname: 'jupiter', upass: 'Passw0rd@2' }
        - { uname: 'neptune', upass: 'Passw0rd@1' }

    - name: 当用户存在添加sudo权限
      shell: grep jupiter  /etc/sudoers && grep neptune /etc/sudoers
      ignore_errors: yes
      register: result
    - name: 添加sudo权限
      lineinfile:
        path: /etc/sudoers
        line: |
         jupiter  ALL=(ALL:ALL) NOPASSWD:ALL
         neptune ALL=(ALL:ALL)  NOPASSWD:ALL
      when: result.rc != 0

    - name: 禁止root远程登录
      lineinfile:
        path: /etc/ssh/sshd_config
        regexp: ^#PermitRootLogin
        line: PermitRootLogin no
      notify: restart sshd

    - name: 备份repo源
      file:
        path: /etc/yum.repos.d/bak
        state: directory
    - name:
      shell:
        cmd: mv /etc/yum.repos.d/*.repo /etc/yum.repos.d/bak/
      ignore_errors: true
    - name: 创建并使用本地yum源
      get_url: 
        url: http://172.22.0.123/repo/Centos-7.repo
        dest: /etc/yum.repos.d/CentOS-Base.repo
    - name: 升级所有 Packages
      yum:
        name: '*'
        state: latest

    - name: 安装firewalld
      yum: 
        name: firewalld
        state: present

    - name: 开启 firewalld
      service:
        name: firewalld
        state: started
        enabled: yes
    - name: 添加22端口来自172.22.0.130的访问
      firewalld:
        rich_rule: "{{item.rules}}"      
        zone: public
        permanent: yes
        immediate: yes
        state: enabled
      loop: 
        - { rules: "rule family=ipv4 source address='172.22.0.130/32' port port=22 protocol=tcp accept" }
        - { rules: "rule family=ipv4 source address='172.22.0.140/32' port port=22 protocol=tcp accept" }

  handlers:
    - name: restart sshd
      service:
        name: sshd
        state: restarted
```

