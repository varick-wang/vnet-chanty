# Openstack删除无用镜像快照

某些镜像快照删除提示错误？

**错误:** 无法删除镜像： undercloud.jasper.com_ok_snapshoot_20210812 。

方法：

1.在openstack点击镜像名称，查看镜像快照的id

例如：05e31685-e93a-40d5-ba63-f25980788d7b

2.在ceph查找镜像的快照

```
rbd snap ls images/05e31685-e93a-40d5-ba63-f25980788d7b
SNAPID NAME                     SIZE TIMESTAMP
   424 BACKUPUTC20211017T161225   0B Sun Oct 17 16:07:45 2021
```

3.删除镜像的快照

```
rbd snap rm images/05e31685-e93a-40d5-ba63-f25980788d7b@BACKUPUTC20211017T161225
Removing snap: 100% complete...done.
```

4.再次在openstack删除镜像快照，可成功删除