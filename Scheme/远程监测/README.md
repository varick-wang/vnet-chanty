# **Remote detection**

### Requirements

illustrate:  
		      &#x2003;Bare metal product equipment is provided to customers for use, and the status of the equipment needs to be monitored remotely.

### Plan

![Remote detection-V4.0.png](https://gitlab.dev.21vianet.com/wang.xuchen/chanty/-/raw/master/Scheme/%E8%BF%9C%E7%A8%8B%E7%9B%91%E6%B5%8B/image/Remote%20detection-V4.0.png)

### Introduction

##### Soultion 1

​		      &#x2003;Install new routers on the bare metal machine room cabinets, configure IPSec-related technology sets, and securely access them through the external network. The cabinet has at least two servers on the shelf, one is configured as a springboard server to provide related service support (for example: yum source) and the other provides zabbix_server master node (including zabbix_database, zabbix_web), deploy zabbix_agent on the bare metal server  
advantage:  
​		      &#x2003;Local area network monitoring, higher safety factor, more accurate real-time data; can be combined with lanproxy to achieve remote access to the intranet zabbix_web service  
shortcoming:  
​		      &#x2003;Occupy the export bandwidth of the external network   
​		      &#x2003;(solution: consider deploying a dedicated communication cable)  



![Remote detection-solution1.drawio ](https://gitlab.dev.21vianet.com/wang.xuchen/chanty/-/raw/master/Scheme/%E8%BF%9C%E7%A8%8B%E7%9B%91%E6%B5%8B/image/soultion1.drawio.png)



##### Soultion 2

​		      &#x2003;Use the routers that have been put on the shelves of the network department to realize remote access to the zabbix master node from the office network through the configured IPSec-related technology set. The cabinet needs to add at least two new servers, one is configured as a springboard server to provide related service support (for example: yum source) and the other provides zabbix_server master node (including zabbix_database, zabbix_web), and deploy zabbix_agent on the bare metal server  
advantage:  
​		      &#x2003;No need to place the bare metal machine room cabinet  
​		      &#x2003;Use existing resources to save costs  
​		      &#x2003;Zabbix master node equipment is safer and more controllable  
shortcoming:  
​		      &#x2003;Compared with the monitoring in the local area network, the transmission rate of the dedicated line is more required, and the export bandwidth is relatively high.  

![Remote detection-solution2.drawio](https://gitlab.dev.21vianet.com/wang.xuchen/chanty/-/raw/master/Scheme/%E8%BF%9C%E7%A8%8B%E7%9B%91%E6%B5%8B/image/soultion2.drawio.png)

##### Soultion 3

​		      &#x2003;Change the monitoring data port to the server's ipmi management port on the basis of 1, 2  
Advantage:  
​		​	 &#x2003;Compared with soultion 1, 2, no need to deploy zabbix_agent on the bare metal server  
shortcoming:  
​		      &#x2003;Different device models and different ipmi & iDrac firmware versions require targeted data capture for different ipmi & iDrac versions  

[zabbix监控的几种方式优缺点](https://gitlab.dev.21vianet.com/wang.xuchen/chanty/-/blob/master/Scheme/%E8%BF%9C%E7%A8%8B%E7%9B%91%E6%B5%8B/zabbix%20%E7%9B%91%E6%8E%A7%E6%96%B9%E5%BC%8F%E4%BC%98%E7%BC%BA%E7%82%B9.md)

##### Soultion 4    
​		      &#x2003;Remote bare machine management using the Neolink Platform hybrid cloud platform.      

​		      &#x2003;Device information can be monitored through ipmi network interface, pxe network interface, and business network interface.  
​		      &#x2003;1. Run Neolink Platform in the virtual machine environment of the Vnet computer room  
​		      &#x2003;2. Install the equipment in the cabinet of the client room to run Neolink Platform  



Advantage:
		      &#x2003;Neolink Platform products are more secure
		      &#x2003;No need to place equipment in the bare metal room cabinet  
		      &#x2003;Use existing resources to save costs
shortcoming:
		      &#x2003;Unknown. May be more dependent on network data transmission bandwidth, requiring a higher transmission rate

![Remote detection-V4.0.png](https://gitlab.dev.21vianet.com/wang.xuchen/chanty/-/raw/master/Scheme/%E8%BF%9C%E7%A8%8B%E7%9B%91%E6%B5%8B/image/Remote%20detection-solution4.drawio.png)
