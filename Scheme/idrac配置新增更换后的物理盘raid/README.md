# dell设备idrac配置新增/更换后的物理盘raid

1. 登录页idrac  
  
![登录](https://gitlab.dev.21vianet.com/wang.xuchen/dell-r540/-/raw/master/Scheme/image/1%E7%99%BB%E5%BD%952021-11-09%20111539.png)

2. 选择配置-存储配置  
  
![配置](https://gitlab.dev.21vianet.com/wang.xuchen/dell-r540/-/raw/master/Scheme/image/2%E9%85%8D%E7%BD%AE_20211109111644.jpg)
    i. 控制器配置查看是否需要清除缓存  
  
![raid准备](https://gitlab.dev.21vianet.com/wang.xuchen/dell-r540/-/raw/master/Scheme/image/raid%E5%87%86%E5%A4%87_20211109112034.jpg)
    
    ii. 物理磁盘配置对应raid  

![虚拟磁盘](https://gitlab.dev.21vianet.com/wang.xuchen/dell-r540/-/raw/master/Scheme/image/%E8%99%9A%E6%8B%9F%E7%A3%81%E7%9B%98_20211109112610.jpg)
  
    iii. 虚拟磁盘配置创建虚拟磁盘盘符（格式化磁盘）  

![创建虚拟磁盘](https://gitlab.dev.21vianet.com/wang.xuchen/dell-r540/-/raw/master/Scheme/image/%E5%88%9B%E5%BB%BA%E8%99%9A%E6%8B%9F%E7%A3%81%E7%9B%98_20211109112909.jpg)  
![格式化虚拟磁盘](https://gitlab.dev.21vianet.com/wang.xuchen/dell-r540/-/raw/master/Scheme/image/%E6%A0%BC%E5%BC%8F%E5%8C%96%E5%88%9B%E5%BB%BA%E7%9A%84%E8%99%9A%E6%8B%9F%E7%A3%81%E7%9B%982021-11-09%20113101.png)  

    
3. 立即应用或下次设备重启应用  
![保存](https://gitlab.dev.21vianet.com/wang.xuchen/dell-r540/-/raw/master/Scheme/image/%E4%BF%9D%E5%AD%9820211109113309.jpg)

END
