# Openstack删除无用卷

##### 某些可用卷总是删不掉？

方法：

1.从openstack复制卷ID

例如：137ec716-e8b1-45d8-9ba0-bdaf4508d773

2.进入ceph，查找卷的快照

```
rbd snap ls volumes/volume-137ec716-e8b1-45d8-9ba0-bdaf4508d773
SNAPID NAME                       SIZE TIMESTAMP
  1708 BACKUPUTC20211012T070524 100GiB Tue Oct 12 07:00:41 2021
```

3.复制快照名称，并进行删除

```
rbd snap remove volumes/volume-137ec716-e8b1-45d8-9ba0-bdaf4508d773@BACKUPUTC20211012T070524
Removing snap: 100% complete...done.
```

4.再次从openstack删除可用卷，等待一段时间后即可删除



