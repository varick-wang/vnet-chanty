
例子
neutron port-update --allowed-address-pair ip_address=172.22.104.200   5fd2c252-f1ed-49dc-bd1c-537c1ffd63d1 ( vm1_ip_port_id ）
neutron port-update --allowed-address-pair ip_address=172.22.104.200   34d2ffc7-8d71-46f1-9fa4-97bf411e97fc （ vm2_ip_port_id ）

neutron port-create --fixed-ip ip_address=172.22.104.200   caea1eda-0259-400b-9f94-ebb1f4c732e3 ( network_id)

neutron security-group-rule-create --protocol 112    48b6b60c-98f9-40dc-8716-e8cc7fd1482b(  security-group-id )

-----------------------------------------------------------------------------------------------------

1.查找IP对应的port id

neutron port-list | grep "172.22.104.131"

2.绑定vip

neutron port-update --allowed-address-pair ip_address=172.22.104.200  port-id

3.设置网络的固定IP

neutron port-create --fixed-ip ip_address=172.22.104.200  network-id

4.开通端口 112 （可不配置）

neutron security-group-rule-create --protocol 112 security-group-id 



