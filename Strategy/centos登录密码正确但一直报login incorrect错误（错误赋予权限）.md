# centos登录密码正确但一直报login incorrect错误（错误赋予权限）             

### 一、先进入单系统用户态

1、在开机启动进入grub菜单界面时，快速按键盘e键，进入编辑界面
2、找到Linux 16的那一行，将ro改为rw init=/sysroot/bin/sh
*如果用的是kvm做了console连接授权，还要把授权的console=ttyS0去掉
3、按下 Control+x ，使用单用户模式启动
4、接着使用下面的命令访问系统

```
chroot /sysroot
```

### 二、修改pam.d文件夹里的login

1、编辑login文件：

```
vi /etc/pam.d/login
```

2、查找文件最后一行是否有session required /lib64/security/pam_limits.so内容且未被注释（行首没有#则未被注释），如果没有该行，则按i进入insert模式添加该内容。
3、如果有session required /lib/security/pam_limits.so或session required pam_limits.so内容，则将其修改为：

```
session required /lib64/security/pam_limits.so
```

 

重启，但还是无法解决问题！！！

 

### 三、修改文件权限

仔细回忆自己无法登录前的操作，想起之前在linux有过文件权限导致出题的经历，尝试查看系统日志 /var/log/secure 的内容。
如果发现有以下内容：

```
localhost login: pam_securetty(login:auth): /etc/securetty is either world writable or not a normal file
```

说明该文件属性已被错误修改，从而导致root无法正常登陆，因此需要修改该文件的属性，以保证只有root用户对该文件拥有一切权限，而对其他用户只有读的权限：

```
chmod 744 /etc/securetty
```

这样修改之后，就可以用正确的root密码登录系统！