# 大数据自动挂载

1. 大数据挂载硬盘较多，通过以下脚本的运行实现挂载入需求

   i. 解决xfs管理2T以上大分区(gpt与mbr问题)

   ii.创建挂载点并开机自动挂载

```shell
[root@ansible-server-1 ansible]# cat gpt.sh
#/bin/bash
num=0
#设置对应的盘符
for i in  b c d e f g h i
do
#更改磁盘为gpt
parted /dev/sd$i <<EOF
mklabel gpt
mkpart primary xfs 0 -1
quit
EOF
echo  'Set gpt successfully'
sleep  2
#格式化磁盘
mkfs.xfs -f /dev/sd$i\1
sleep  5
#创建对应挂载点
i=$i\1
let num=$num+1
mkdir /data$num
#写入自动挂载
echo "/dev/sd$i  /data$num  xfs defaults  0  0" >> /etc/fstab
done
mount -a
```

2. 服务器数量较多时，利用ansible运行

   测试设备的连通性

```bash
[root@ansible-server-1 ansible]# ansible b28r107bigdata --list-hosts
  hosts (3):
    b28r107bigdata01
    b28r107bigdata02
    b28r107bigdata03
    
[root@ansible-server-1 ansible]# ansible b28r107bigdata -m ping 
b28r107bigdata01 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    }, 
    "changed": false, 
    "ping": "pong"
}
b28r107bigdata03 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    }, 
    "changed": false, 
    "ping": "pong"
}
b28r107bigdata02 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    }, 
    "changed": false, 
    "ping": "pong"
}
```

​	利用ansible ad-hoc中的script模块，script模块会把-a后面的脚本拷贝到被管理端主机，然后执行这个脚本。

```bash
[root@ansible-server-1 ansible]# ansible b28r107bigdata -m script -a '/root/ansible/gpt.sh' 
```

![image-20211019135331256](D:\VK\git\chanty\Scheme\大数据测试机装机问题\Picture illustration\image-20211019135331256.png)

3. 查看挂载情况

```bash
[root@ansible-server-1 ansible]# ansible b28r107bigdata -m shell -a 'lsblk'
b28r107bigdata03 | CHANGED | rc=0 >>
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0  3.7T  0 disk 
├─sda1            8:1    0    1M  0 part 
├─sda2            8:2    0    1G  0 part /boot
└─sda3            8:3    0  3.7T  0 part 
  ├─centos-root 253:0    0  3.5T  0 lvm  /
  └─centos-swap 253:1    0  128G  0 lvm  [SWAP]
sdb               8:16   0  3.7T  0 disk 
└─sdb1            8:17   0  3.7T  0 part /data5
sdc               8:32   0  3.7T  0 disk 
└─sdc1            8:33   0  3.7T  0 part /data6
sdd               8:48   0  3.7T  0 disk 
└─sdd1            8:49   0  3.7T  0 part /data7
sde               8:64   0  3.7T  0 disk 
└─sde1            8:65   0  3.7T  0 part /data8
sdf               8:80   0  3.7T  0 disk 
└─sdf1            8:81   0  3.7T  0 part /data9
sdg               8:96   0  3.7T  0 disk 
└─sdg1            8:97   0  3.7T  0 part /data10
sdh               8:112  0  3.7T  0 disk 
└─sdh1            8:113  0  3.7T  0 part /data11
sdi               8:128  0  3.7T  0 disk 
└─sdi1            8:129  0  3.7T  0 part /data12
#其中一台为例
```

