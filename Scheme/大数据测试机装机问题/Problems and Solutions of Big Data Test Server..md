# centos7.9 系统默认安装 / 分区扩容问题

思路：

​	查看现有分区--->lvm更改需要减少的分区--->lvm将腾出分区添加至需扩容分区

```bash
[root@b28r102test03 ~]# lsblk			##查看现有分区
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0  3.7T  0 disk 
├─sda1            8:1    0    1M  0 part 
├─sda2            8:2    0    1G  0 part /boot
└─sda3            8:3    0  3.7T  0 part 
  ├─centos-root 253:0    0   50G  0 lvm  /				##等待扩容
  ├─centos-swap 253:1    0    4G  0 lvm  [SWAP]
  └─centos-home 253:2    0  3.6T  0 lvm  /home			##设置大小为100G（或删除分区）
sdb               8:16   0  3.7T  0 disk 
sdc               8:32   0  3.7T  0 disk 
sdd               8:48   0  3.7T  0 disk 
sde               8:64   0  3.7T  0 disk 
sdf               8:80   0  3.7T  0 disk 
sdg               8:96   0  3.7T  0 disk
[root@b28r102test03 ~]# lvresize -L 100G /dev/centos/home		#设置home大小为100G
[root@b28r102test03 ~]# xfs_growfs /dev/mapper/centos-home		#刷新xfs文件系统信息
[root@b28r102test03 ~]# lvextend -l +100%FREE /dev/centos/root	#将所有未分配添加至 /
[root@b28r102test03 ~]# xfs_growfs  /dev/mapper/centos-root		#刷新xfs文件系统信息
[root@b28r102test03 ~]# lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0  3.7T  0 disk 
├─sda1            8:1    0    1M  0 part 
├─sda2            8:2    0    1G  0 part /boot
└─sda3            8:3    0  3.7T  0 part 
  ├─centos-root 253:0    0  3.5T  0 lvm  /					##查看扩容状态
  ├─centos-swap 253:1    0    4G  0 lvm  [SWAP]
  └─centos-home 253:2    0  100G  0 lvm  /home
sdb               8:16   0  3.7T  0 disk 
sdc               8:32   0  3.7T  0 disk 
sdd               8:48   0  3.7T  0 disk 
sde               8:64   0  3.7T  0 disk 
sdf               8:80   0  3.7T  0 disk 
sdg               8:96   0  3.7T  0 disk 
[root@b28r102test03 ~]# df -hT 
文件系统                类型      容量  已用  可用 已用% 挂载点
devtmpfs                devtmpfs   63G     0   63G    0% /dev
tmpfs                   tmpfs      63G   12K   63G    1% /dev/shm
tmpfs                   tmpfs      63G   11M   63G    1% /run
tmpfs                   tmpfs      63G     0   63G    0% /sys/fs/cgroup
/dev/mapper/centos-root xfs       3.5T  8.9G  3.5T    1% /
/dev/sda2               xfs      1014M  150M  865M   15% /boot
/dev/mapper/centos-home xfs       3.6T   33M  3.6T    1% /home
tmpfs                   tmpfs      13G     0   13G    0% /run/user/0
```

成功。

---

# xfs管理2T以上大分区(gpt与mbr问题)

1. 设置gpt硬盘（sdb为例）

```bash
[root@b28r102test03 ~]# parted /dev/sdb
(parted) mklabel gpt 
(parted) mkpart primary xfs 0 -1   (Ignore)
(parted) quit
```

2. 安装xfs工具

```bash
[root@b28r102test03 ~]# yum install xfsprogs
```

3. 格式化xfs分区

```bash
[root@b28r102test03 ~]# mkfs.xfs -f /dev/sdb1
```

4. 查看xfs分区uuid

```bash
[root@b28r102test03 ~]# ls -lh /dev/disk/by-uuid
```

5. 更新/etc/fstab（挂载点以/data为例）

```bash
[root@b28r102test03 ~]# vim /etc/fastab

......
# uuid         /挂载绝对路径       文件系统      defaults    0  0
UUID=62ca3df2-4ad4-43ea-94ba-0c8999eeafd3 /data  xfs    defaults        1 2
......

```

6. 挂载新分区(执行一次/etc/fastab下的所有挂载)

```bash
[root@b28r102test03 ~]# mount -a
```

7. 强制卸载/data分区(可不执行)

```bash
[root@b28r102test03 ~]# mount -l /data
```



---



# rsync同步公网yum源搭建本地yum仓库

## (内网环境使用外网yum源问题)	

同步公网 yum 源，上游 yum 源必须要支持 rsync 协议，否则不能使用 rsync 进行同步。

*CentOS源*：rsync://rsync.mirrors.ustc.edu.cn/centos/
 *EPEL源*：rsync://rsync.mirrors.ustc.edu.cn/epel/

**同步CentOS7**

```bash
# 同步 epel
$ rsync -vrt --bwlimit=3000 --exclude=debug/ rsync://rsync.mirrors.ustc.edu.cn/epel/7/x86_64/ /data/mirrors/centos/epel7/x86_64/
 _______________________________________________________________
|         University of Science and Technology of China         |
|           Open Source Mirror  (mirrors.ustc.edu.cn)           |
|===============================================================|
|                                                               |
| Debian primary mirror in China mainland (ftp.cn.debian.org),  |
|     also mirroring a great many OSS projects & Linux distros. |
|                                                               |
| Currently we don't limit speed. To prevent overload, Each IP  |
| is only allowed to start upto 2 concurrent rsync connections. |
|                                                               |
| This site also provides http/https/ftp access.                |
|                                                               |
| Supported by USTC Network Information Center                  |
|          and USTC Linux User Group (http://lug.ustc.edu.cn/). |
|                                                               |
|    Sync Status:  https://mirrors.ustc.edu.cn/status/          |
|           News:  https://servers.ustclug.org/                 |
|        Contact:  lug@ustc.edu.cn                              |
|                                                               |
|_______________________________________________________________|


receiving incremental file list
./
x86_64/
x86_64/0/
x86_64/0/0ad-0.0.22-1.el7.x86_64.rpm
x86_64/0/0ad-data-0.0.22-1.el7.noarch.rpm
...

# 同步 base
$ rsync -vrt rsync://mirrors.ustc.edu.cn/centos/7/os/x86_64/ /data/mirrors/centos/7/os/x86_64/

# 同步 extras
$ rsync -vrt rsync://mirrors.ustc.edu.cn/centos/7/extras/x86_64/ /data/mirrors/centos/7/extras/x86_64/

# 同步 updates
$ rsync -vrt rsync://mirrors.ustc.edu.cn/centos/7/updates/x86_64/ /data/mirrors/centos/7/updates/x86_64/
```

**同步 CentOS6**

```bash
$ rsync -vrt --bwlimit=20000 rsync://rsync.mirrors.ustc.edu.cn/centos/6.9/ /data/mirrors/centos/6.9/

$ rsync -vrt --bwlimit=20000 --exclude=debug/ rsync://rsync.mirrors.ustc.edu.cn/epel/6/x86_64/ /data/mirrors/centos/epel6/x86_64/
```

配置 apache 发布镜像目录，当然也可以用 nginx。

```bash
$ vim /etc/httpd/conf/httpd.conf

# 注释掉这一段
# <Directory />
   # AllowOverride none
   # Require all denied
# </Directory>

# 添加这一段
Alias /centos "/data/mirrors/centos/"
<Directory "/data/mirrors/centos">
    Options Indexes MultiViews FollowSymLinks
    AllowOverride None
    Order allow,deny
    Allow from all
</Directory>

$ /etc/init.d/httpd reload
```

最后，配置repo就可以使用了。

```bash
[base]
name=CentOS-$releasever - Base
baseurl=http://10.100.10.89/centos/$releasever/os/$basearch/
enable=1
gpgcheck=0

#released updates
[updates]
name=CentOS-$releasever - Updates
baseurl=http://10.100.10.89/centos/$releasever/updates/$basearch/
enable=1
gpgcheck=0

#additional packages that may be useful
[extras]
name=CentOS-$releasever - Extras
baseurl=http://10.100.10.89/centos/$releasever/extras/$basearch/
enable=1
gpgcheck=0

[epel]
name=Extra Packages for Enterprise Linux 7 - $basearch
baseurl=http://10.100.10.89/centos/epel/7/$basearch
enabled=1
gpgcheck=0
```



