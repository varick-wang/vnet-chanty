# zabbix_agentd 自动安装部署

```bash
[root@ansible-server-1 ansible]# ansible b28r103bdt -m copy -a 'src=/mnt/zabbix dest=/mnt/' 		#拷贝相关文件和配置文件

/mnt/zabbix/zabbix-4.0.33.tar.gz  
/mnt/zabbix/zabbix_agentd.conf  
/mnt/zabbix/zabbix_install.sh

[root@ansible-server-1 ansible]# vim /mnt/zabbix/zabbix_install.sh	#查看脚本
#/bin/bash
#添加YUM源
##备份
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
##下载本地yum的配置文件
curl -o /etc/yum.repos.d/CentOS-Base.repo http://172.22.50.244/repo/Centos-7.repo
##生成缓存
yum makecache

sleep 1
agent1=/usr/local/etc/zabbix_agentd.conf
agent2=/mnt/zabbix/zabbix_agentd.conf

#关闭selinux
sed -i "s/SELINUX=enforcing/SELINUX=disabled/"  /etc/selinux/config
setenforce 0
#创建user——zabbix
useradd -s /sbin/nologin zabbix
#安装基础环境
yum -y install gcc pcre-devel autoconf
#解压并编码安装zabbix_agentd
cd /mnt/zabbix
tar xf ./zabbix-4.0.33.tar.gz
cd zabbix-4.0.33
./configure --enable-agent
make && make install
#复制模板配置文件，更改配置文件对应的主机名
cp $agent2 $agent1
sed -i "s/Hostname=Zabbix_agent/Hostname=Zabbix_agent_$HOSTNAME/"  $agent1
#开启agentd服务
zabbix_agentd
~                    
```

技术有限，只实现脚本方式自动化部署。

后期实现ansible-playbook yaml文件部署