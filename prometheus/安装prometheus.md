# 安装prometheus

采用容器运行prometheus:

```bash
#  docker run -id --name prometheus -p 9090:9090 -v /etc/prometheus/:/etc/prometheus/ prom/prometheus
```

安装grafana：

```bash
# docker run --restart=always -id --name grafana -p 3000:3000 -e "GF_SECURITY_ADMIN_PASSWORD=admin" -v "/etc/grafana/:/etc/grafana/" -v "/var/lib/grafana:/var/lib/grafana" grafana/grafana
```

